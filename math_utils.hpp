#include "mesh.hpp"
#include <iostream>


float xStep(float width, int widthNum, int j)
{
	return width / widthNum;
}

float yStep(float height, int heightNum, int i)
{
	return height / heightNum;
}


std::pair<vector<Vertex>, vector<unsigned int>> generatePlane(float width, float height, int widthNum, int heightNum)
{
	vector<Vertex> vertices;
	vector<unsigned int> indices;

	float x = -width / 2;
	float y = -height / 2;

	for (int i = 0; i <= heightNum; i++)
	{
		for (int j = 0; j <= widthNum; j++)
		{
			Vertex vertex;
			vertex.Position = glm::vec3(x, 0, y);
			vertex.Normal = glm::vec3(0, 1, 0);
			vertex.TexCoords = glm::vec2((float)j / widthNum, (float)i / heightNum);
			vertices.push_back(vertex);

			x += xStep(width, widthNum, j);
		}
		x = -width / 2;
		y += yStep(height, heightNum, i);
	}

	for (int i = 0; i < heightNum; i++)
	{
		for (int j = 0; j < widthNum; j++)
		{
			indices.push_back(i * (widthNum + 1) + j);
			indices.push_back((i + 1) * (widthNum + 1) + j);
			indices.push_back(i * (widthNum + 1) + j + 1);

			indices.push_back((i + 1) * (widthNum + 1) + j);
			indices.push_back((i + 1) * (widthNum + 1) + j + 1);
			indices.push_back(i * (widthNum + 1) + j + 1);
		}
	}

	return std::make_pair<std::vector<Vertex>, std::vector<unsigned int>>(std::move(vertices), std::move(indices));
}

std::pair<vector<Vertex>, vector<unsigned int>> bublePlain(std::pair<vector<Vertex>, vector<unsigned int>> plane, float centerX, float centerZ, float bubleH, float bubleR)
{
	vector<Vertex> vertices = plane.first;
	vector<unsigned int> indices = plane.second;

	for (int i = 0; i < vertices.size(); i++)
	{
		float x = vertices[i].Position.x;
		float z = vertices[i].Position.z;

		float distance = sqrt(pow(x - centerX, 2) + pow(z - centerZ, 2));

		if (distance < bubleR)
		{
			float height = bubleH * (1 - pow(distance / bubleR, 2));
			if (height > vertices[i].Position.y) {
				vertices[i].Position.y = height;
				float dSlightlyRight = sqrt(pow(x + 0.01 - centerX, 2) + pow(z - centerZ, 2));
				float heightSlightlyRight = bubleH * (1 - pow(dSlightlyRight / bubleR, 2));
				float dSlightlyFront = sqrt(pow(x - centerX, 2) + pow(z + 0.01 - centerZ, 2));
				float heightSlightlyFront = bubleH * (1 - pow(dSlightlyFront / bubleR, 2));
				vertices[i].Normal = glm::normalize(
					glm::cross(
						glm::vec3(0, heightSlightlyFront - height, 0.01),
						glm::vec3(0.01, heightSlightlyRight - height, 0)
					)
				);
			}
		}
	}

	return std::make_pair<std::vector<Vertex>, std::vector<unsigned int>>(std::move(vertices), std::move(indices));
}

std::pair<vector<Vertex>, vector<unsigned int>> generateSphere(float radius, int sectorCount, int stackCount, bool reverse=false)
{
	vector<Vertex> vertices;
	vector<unsigned int> indices;

	float x, y, z, xy;                              // vertex position
	float nx, ny, nz, lengthInv = 1.0f / radius;    // vertex normal
	float s, t;                                     // vertex texCoord

	float sectorStep = 2 * glm::pi<float>() / sectorCount;
	float stackStep = glm::pi<float>() / stackCount;
	float sectorAngle, stackAngle;

	for (int i = 0; i <= stackCount; i++)
	{
		stackAngle = glm::pi<float>() / 2 - i * stackStep;        // starting from pi/2 to -pi/2
		xy = radius * cosf(stackAngle);             // r * cos(u)
		z = radius * sinf(stackAngle);              // r * sin(u)

		// add (sectorCount+1) vertices per stack
		// the first and last vertices have same position and normal, but different tex coords
		for (int j = 0; j <= sectorCount; j++)
		{
			sectorAngle = j * sectorStep;           // starting from 0 to 2pi

			// vertex position (x, y, z)
			x = xy * cosf(sectorAngle);             // r * cos(u) * cos(v)
			y = xy * sinf(sectorAngle);             // r * cos(u) * sin(v)
			Vertex vertex;
			vertex.Position = glm::vec3(x, y, z);
			vertex.Normal = glm::vec3(x * lengthInv, y * lengthInv, z * lengthInv);
			vertex.TexCoords = glm::vec2((float)j / sectorCount, (float)i / stackCount);
			vertices.push_back(vertex);
		}
	}

	// generate CCW index list of sphere triangles
	unsigned int k1, k2;
	for (int i = 0; i < stackCount; i++)
	{
		k1 = i * (sectorCount + 1);     // beginning of current stack
		k2 = k1 + sectorCount + 1;      // beginning of next stack

		for (int j = 0; j < sectorCount; j++, k1++, k2++)
		{
			// 2 triangles per sector excluding first and last stacks
			// k1 => k2 => k1+1
			if (i != 0)
			{
				indices.push_back(k1);
				if (!reverse)
				{
					indices.push_back(k2);
					indices.push_back(k1 + 1);
				}
				else
				{
					indices.push_back(k1 + 1);
					indices.push_back(k2);
				}
			}

			// k1+1 => k2 => k2+1
			if (i != (stackCount - 1))
			{
				indices.push_back(k1 + 1);
				if (!reverse)
				{
					indices.push_back(k2);
					indices.push_back(k2 + 1);
				}
				else
				{
					indices.push_back(k2 + 1);
					indices.push_back(k2);
				}
			}
		}
	}
	return std::make_pair<std::vector<Vertex>, std::vector<unsigned int>>(std::move(vertices), std::move(indices));
}

std::pair<vector<Vertex>, vector<unsigned int>> generateCube(float width, float height, float depth)
{
	vector<Vertex> vertices;
	vector<unsigned int> indices;

	// front
	Vertex vertex;
	vertex.Position = glm::vec3(-width / 2, height / 2, depth / 2);
	vertex.Normal = glm::vec3(0, 0, 1);
	vertex.TexCoords = glm::vec2(0, 0);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(width / 2, height / 2, depth / 2);
	vertex.Normal = glm::vec3(0, 0, 1);
	vertex.TexCoords = glm::vec2(1, 0);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(width / 2, -height / 2, depth / 2);
	vertex.Normal = glm::vec3(0, 0, 1);
	vertex.TexCoords = glm::vec2(1, 1);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(-width / 2, -height / 2, depth / 2);
	vertex.Normal = glm::vec3(0, 0, 1);
	vertex.TexCoords = glm::vec2(0, 1);
	vertices.push_back(vertex);

	// back
	vertex.Position = glm::vec3(-width / 2, height / 2, -depth / 2);
	vertex.Normal = glm::vec3(0, 0, -1);
	vertex.TexCoords = glm::vec2(0, 0);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(width / 2, height / 2, -depth / 2);
	vertex.Normal = glm::vec3(0, 0, -1);
	vertex.TexCoords = glm::vec2(1, 0);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(width / 2, -height / 2, -depth / 2);
	vertex.Normal = glm::vec3(0, 0, -1);
	vertex.TexCoords = glm::vec2(1, 1);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(-width / 2, -height / 2, -depth / 2);
	vertex.Normal = glm::vec3(0, 0, -1);
	vertex.TexCoords = glm::vec2();

	// left
	vertex.Position = glm::vec3(-width / 2, height / 2, depth / 2);
	vertex.Normal = glm::vec3(-1, 0, 0);
	vertex.TexCoords = glm::vec2(0, 0);
	vertices.push_back(vertex);


	vertex.Position = glm::vec3(-width / 2, height / 2, -depth / 2);
	vertex.Normal = glm::vec3(-1, 0, 0);
	vertex.TexCoords = glm::vec2(1, 0);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(-width / 2, -height / 2, -depth / 2);
	vertex.Normal = glm::vec3(-1, 0, 0);
	vertex.TexCoords = glm::vec2(1, 1);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(-width / 2, -height / 2, depth / 2);
	vertex.Normal = glm::vec3(-1, 0, 0);
	vertex.TexCoords = glm::vec2(0, 1);
	vertices.push_back(vertex);

	// right
	vertex.Position = glm::vec3(width / 2, height / 2, depth / 2);
	vertex.Normal = glm::vec3(1, 0, 0);
	vertex.TexCoords = glm::vec2(0, 0);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(width / 2, height / 2, -depth / 2);
	vertex.Normal = glm::vec3(1, 0, 0);
	vertex.TexCoords = glm::vec2(1, 0);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(width / 2, -height / 2, -depth / 2);
	vertex.Normal = glm::vec3(1, 0, 0);
	vertex.TexCoords = glm::vec2(1, 1);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(width / 2, -height / 2, depth / 2);
	vertex.Normal = glm::vec3(1, 0, 0);
	vertex.TexCoords = glm::vec2(0, 1);
	vertices.push_back(vertex);

	// top
	vertex.Position = glm::vec3(-width / 2, height / 2, depth / 2);
	vertex.Normal = glm::vec3(0, 1, 0);
	vertex.TexCoords = glm::vec2(0, 0);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(width / 2, height / 2, depth / 2);
	vertex.Normal = glm::vec3(0, 1, 0);
	vertex.TexCoords = glm::vec2(1, 0);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(width / 2, height / 2, -depth / 2);
	vertex.Normal = glm::vec3(0, 1, 0);
	vertex.TexCoords = glm::vec2(1, 1);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(-width / 2, height / 2, -depth / 2);
	vertex.Normal = glm::vec3(0, 1, 0);
	vertex.TexCoords = glm::vec2(0, 1);
	vertices.push_back(vertex);

	// bottom
	vertex.Position = glm::vec3(-width / 2, -height / 2, depth / 2);
	vertex.Normal = glm::vec3(0, -1, 0);
	vertex.TexCoords = glm::vec2(0, 0);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(width / 2, -height / 2, depth / 2);
	vertex.Normal = glm::vec3(0, -1, 0);
	vertex.TexCoords = glm::vec2(1, 0);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(width / 2, -height / 2, -depth / 2);
	vertex.Normal = glm::vec3(0, -1, 0);
	vertex.TexCoords = glm::vec2(1, 1);
	vertices.push_back(vertex);

	vertex.Position = glm::vec3(-width / 2, -height / 2, -depth / 2);
	vertex.Normal = glm::vec3(0, -1, 0);
	vertex.TexCoords = glm::vec2(0, 1);
	vertices.push_back(vertex);

	// front
	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);
	indices.push_back(0);
	indices.push_back(2);
	indices.push_back(3);

	// back
	indices.push_back(4);
	indices.push_back(5);
	indices.push_back(6);
	indices.push_back(4);
	indices.push_back(6);
	indices.push_back(7);

	// left
	indices.push_back(8);
	indices.push_back(9);
	indices.push_back(10);
	indices.push_back(8);
	indices.push_back(10);
	indices.push_back(11);

	// right
	indices.push_back(12);
	indices.push_back(13);
	indices.push_back(14);
	indices.push_back(12);
	indices.push_back(14);
	indices.push_back(15);

	// top
	indices.push_back(16);
	indices.push_back(17);
	indices.push_back(18);
	indices.push_back(16);
	indices.push_back(18);

	indices.push_back(19);

	// bottom
	indices.push_back(20);
	indices.push_back(21);
	indices.push_back(22);
	indices.push_back(20);
	indices.push_back(22);
	indices.push_back(23);

	return std::make_pair<std::vector<Vertex>, std::vector<unsigned int>>(std::move(vertices), std::move(indices));
}