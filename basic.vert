#version 330 core
layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inUV;

out vec3 chFragPos;
out vec3 chNormal;
out vec2 chUV;

uniform mat4 uM;
uniform mat4 uV;
uniform mat4 uP;

#define WATER_IRRITANTS 5
uniform vec4 uWaterIrritants[WATER_IRRITANTS];  // water irritant z - radius, w - power
uniform float uWavingFrequency;
uniform float uWavePhaze;
uniform float uWaveAmplitude;

uniform int uFlags;
#define NAME_MODE     64

float countWave(vec3 pos)
{
	float result = 0.0;
	for (int i = 0; i < WATER_IRRITANTS; i++)
	{
		float distance = max(0, length(pos.xz - uWaterIrritants[i].xy) - uWaterIrritants[i].z);
		float distanceK = max(1.0, distance);
		float waterIrritantPower = uWaterIrritants[i].w / (distanceK * distanceK);
		float distanceSupressed = distance;
		if (distanceSupressed > 1.0)
			distanceSupressed = 1.0 + log2(distance);
		float wave = sin(distanceSupressed * uWavingFrequency - uWavePhaze) * waterIrritantPower * uWaveAmplitude;
		result += wave;
	}
	return result;
}

void main()
{
    chUV = inUV;

	if (uFlags & NAME_MODE) {
		chFragPos = vec3(uM * vec4(inPos, 1.0));
		gl_Position = uP * uV * vec4(chFragPos, 1.0);
		return;
	}

    chFragPos = vec3(uM * vec4(inPos, 1.0));
    chNormal = mat3(transpose(inverse(uM))) * inNormal;  
    

	vec3 slightlyRight = chFragPos + vec3(0.001, 0, 0);
	vec3 slightlyForward = chFragPos + vec3(0, 0, 0.001);

	float wave = countWave(chFragPos);
	float waveRight = countWave(slightlyRight);
	float waveForward = countWave(slightlyForward);

	if (abs(wave) + abs(waveRight) + abs(waveForward) > 0.00001)
	{
		chFragPos.y += wave;
		slightlyRight.y += countWave(slightlyRight);
		slightlyForward.y += countWave(slightlyForward);

		chNormal = normalize(cross(chFragPos - slightlyForward, chFragPos - slightlyRight));
		if (chNormal.y < 0)
			chNormal = -chNormal;
	}

    gl_Position = uP * uV * vec4(chFragPos, 1.0);
}

