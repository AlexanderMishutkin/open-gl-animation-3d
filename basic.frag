#version 330 core
out vec4 FragColor;

in vec3 chNormal;
vec3 actualNormal;
in vec3 chFragPos;  
in vec2 chUV;
  
uniform vec3 uLightPos; 
uniform vec3 uViewPos; 
uniform vec3 uLightColor;
uniform vec3 uSkyColor;
uniform vec3 uAmbient;

uniform int uFlags;

#define SHOW_NORMALS   1
#define SHOW_GRID      2
#define BLINN          4
#define SHOW_CUTOFF    8
#define WATER_SHADING 16
#define SKY_MODE      32
#define NAME_MODE     64

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;

struct PointLight {    
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;  

    vec3 lightColor;

    vec3  direction;
    float cutOff;
    float outerCutOff;
};  
#define NR_POINT_LIGHTS 2  
uniform PointLight pointLights[NR_POINT_LIGHTS];

// ------------  Debug  --------------
bool drawDebugGrid() {
    if (uFlags & SHOW_GRID)
	{
        if (pow(chFragPos.x, 2) + pow(chFragPos.z, 2) + pow(chFragPos.y, 2) < 0.05) {
            FragColor = vec4(0.9, 0.0, 0.9, 1.0);
		    return true;
		}
        if (mod(chFragPos.x, 1.0) < 0.01) {
			FragColor = vec4(0.8, 0.1, 0.1, 1.0);
		    return true;
        }
        if (mod(chFragPos.z, 1.0) < 0.01) {
			FragColor = vec4(0.1, 0.8, 0.1, 1.0);
		    return true;
		}
        if (mod(chFragPos.y, 1.0) < 0.01) {
            FragColor = vec4(0.1, 0.1, 0.8, 1.0);
            return true;
        }
	}
    return false;
}

// ------------  Blinn-Phong  --------------
vec3 dirLight(vec3 lightPos, vec3 lightColor) {
    vec3 color = texture(texture_diffuse1, chUV).rgb;

    vec3 ambient = color * uAmbient;

    // diffuse
    vec3 lightDir = normalize(lightPos - chFragPos);
    vec3 normal = normalize(actualNormal);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * color;

    // specular
    vec3 viewDir = normalize(uViewPos - chFragPos);
    float spec = 0.0;
    if(uFlags & BLINN)
    {
        vec3 halfwayDir = normalize(lightDir + viewDir);  
        spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
    }
    else
    {
        vec3 reflectD = reflect(-lightDir, normal);
        spec = pow(max(dot(viewDir, reflectD), 0.0), 8.0);
    }
    vec3 specular = texture(texture_specular1, chUV).rgb * spec;
    // vec3 specular = vec3(1.0, 1.0, 1.0) * spec;
    return ambient + (diffuse + specular) * lightColor;
}

vec3 pointLight() {
	vec3 result = vec3(0.0);
    for(int i = 0; i < NR_POINT_LIGHTS; i++)
	{
        PointLight light = pointLights[i];
        vec3 direction = light.position - chFragPos;
        float distance    = length(direction);
        float attenuation = 1.0 / (light.constant + light.linear * distance + 
    		            light.quadratic * (distance * distance));
		vec3 exactLight = dirLight(pointLights[i].position, pointLights[i].lightColor) * attenuation;

        float theta     = dot(normalize(direction), normalize(-pointLights[i].direction));


        if (uFlags & SHOW_CUTOFF) {
            if (i == 0) result += vec3(1.0, 0.0, 0.0) * theta;
            if (i == 1) result += vec3(0.0, 1.0, 0.0) * theta;
            if (i >  1) result += vec3(0.0, 0.0, 1.0) * theta;
		} else {
            if (theta < light.outerCutOff) continue;
            if (theta < light.cutOff) {
                float intensity = (theta - light.outerCutOff) / (light.cutOff - light.outerCutOff);
				exactLight *= intensity;
            }
            result += exactLight;
        }
	}
    return result;
}

void debugNormals() {
	if (uFlags & SHOW_NORMALS)
	{
		FragColor = vec4(actualNormal, 1.0);
		return;
	}
}

#define WATER_IRRITANTS 5
uniform vec4 uWaterIrritants[WATER_IRRITANTS];  // water irritant: xy - xz, z - radius, w - power
uniform float uWavingFrequency;
uniform float uWavePhaze;
uniform float uWaveAmplitude;

float countWave(vec3 pos)
{
	float result = 0.0;
	for (int i = 0; i < WATER_IRRITANTS; i++)
	{
		float distance = max(0, length(pos.xz - uWaterIrritants[i].xy) - uWaterIrritants[i].z);
		float distanceK = max(1.0, distance);
		float waterIrritantPower = uWaterIrritants[i].w / (distanceK * distanceK);
		float distanceSupressed = distance;
		if (distanceSupressed > 1.0)
			distanceSupressed = 1.0 + log2(distance);
		float wave = sin(distanceSupressed * uWavingFrequency - uWavePhaze) * waterIrritantPower * uWaveAmplitude;
		result += wave;
	}
	return result;
}

vec3 approxNormal(vec3 pos)
{
    if ((uFlags & WATER_SHADING) == 0)
		return chNormal;
    if (WATER_IRRITANTS == 0)
		return chNormal;
    if (uWaveAmplitude < 0.00001)
        return chNormal;
	
    pos.y = 0.0;
    vec3 slightlyRight = pos + vec3(0.001, 0, 0);
	vec3 slightlyForward = pos + vec3(0, 0, 0.001);

	float wave = countWave(pos);
	float waveRight = countWave(slightlyRight);
	float waveForward = countWave(slightlyForward);

	if (abs(wave) + abs(waveRight) + abs(waveForward) > 0.00001)
	{
		pos.y += wave;
		slightlyRight.y += countWave(slightlyRight);
		slightlyForward.y += countWave(slightlyForward);

		vec3 normal = normalize(cross(pos - slightlyForward, pos - slightlyRight));
		if (chNormal.y < 0)
			normal = -normal;
        return normal;
	}
    return chNormal;
}


void main()
{
    // ---------------  Name  --------------------
	if (uFlags & NAME_MODE) {
        if (texture(texture_diffuse1, chUV).a < 0.1) discard; else {
		    FragColor = vec4(texture(texture_diffuse1, chUV));
		    return;
        }
	}

    // ---------------  Debug  --------------------
    if (drawDebugGrid()) return;

    // ---------------  Normals  ------------------
    actualNormal = chNormal;
    actualNormal = approxNormal(chFragPos);

    // --------------  Lighting  ------------------
    float a = texture(texture_diffuse1, chUV).a;
    FragColor = vec4(dirLight(uLightPos, uLightColor) + pointLight(), a);

    // -----------------  Sky  --------------------
    if (uFlags & SKY_MODE)
	{
        FragColor = vec4(uSkyColor, 1.0) + 
            max(
                vec4(0.0), 
                texture(texture_diffuse1, chUV) * 
                    (1 - pow(length(uSkyColor), 0.5)
            ) +
            vec4(uLightColor, 1.0) * (1 / length(uLightPos - chFragPos))
        );

	}
    // FragColor = vec4(pointLight(), 1.0);

    if (texture(texture_diffuse1, chUV).a < 0.1) discard;

    // ------------  Debug  --------------
    debugNormals();
}

