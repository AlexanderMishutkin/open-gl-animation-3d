//Opis: Primjer ucitavanja modela upotrebom ASSIMP biblioteke
//Preuzeto sa learnOpenGL

#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <bitset>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader.hpp"
#include "model.hpp"
#include "mesh.hpp"
#include "math_utils.hpp"

const unsigned int wWidth = 1600;
const unsigned int wHeight = 1024;

#define SHOW_NORMALS   1
#define SHOW_GRID      2
#define BLINN 	       4
#define SHOW_CUTOFF    8
#define WATER_SHADING 16
#define SKY_MODE      32
#define NAME_MODE     64

#define TOGLE 0.2f
#define TIME_SPEED 20.0f
#define TIME_PHAZE glm::radians(270.0f)

float lastSwitch = 0.0f;

void handleInput(
    GLFWwindow* window, Shader& universalShader, glm::vec3& eye, glm::vec3& center, glm::vec3& up, glm::mat4& projectionP, 
    glm::mat4& projectionO, const float delta, bool& isometric, float& orthoScale, int& flags, float& t
);

glm::vec3 lightFromTime(const float t)
{
    float sunDegree = t / TIME_SPEED + TIME_PHAZE;
    if (std::fmodf(glm::degrees(sunDegree), 360.0) > 180.0)
    {
        return glm::vec3(
            1.0f * std::powf(-std::sinf(sunDegree), 1.0f),
            1.0f * std::powf(-std::sinf(sunDegree), 2.0f),
            0.5f * std::powf(-std::sinf(sunDegree), 2.0f)
        );
    }
    else
    {
        return glm::vec3(0.0f);
    }
}

glm::vec3 skyFromTime(const float t)
{
    float sunDegree = t / TIME_SPEED + TIME_PHAZE;
    if (std::fmodf(glm::degrees(sunDegree), 360.0) > 180.0)
	{
		return glm::vec3(
            0.8f * std::powf(-std::sinf(sunDegree), 1.0f), 
            0.9f * std::powf(-std::sinf(sunDegree), 2.0f),
            1.0f * std::powf(-std::sinf(sunDegree), 2.0f)
        );
	}
	else
	{
		return glm::vec3(0.0f);
	}
}

float cloudShift(const float t, const float shift)
{
	return -100.0f + std::fmodf(t / 4.0f + shift, 200.0f);
}

int main()
{
    if(!glfwInit())
    {
        std::cout << "GLFW fail!\n" << std::endl;
        return -1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);
    
    float ratio = (float)mode->width / (float)mode->height;
    GLFWwindow* window = glfwCreateWindow(mode->width, mode->height, "My Title", monitor, NULL);

    // GLFWwindow* window = glfwCreateWindow(wWidth, wHeight, "LearnOpenGL", NULL, NULL);

    if (window == NULL)
    {
        std::cout << "Window fail!\n" << std::endl;
        glfwTerminate();
        return -2;
    }
    glfwMakeContextCurrent(window);

    if (glewInit() !=GLEW_OK)
    {
        std::cout << "GLEW fail! :(\n" << std::endl;
        return -3;
    }

    unsigned int waterTextureSpecId = TextureFromFile("water_spec.png", "res");
    Mesh::init(waterTextureSpecId, Texture(waterTextureSpecId, string("texture_specular"), string("res/water_spec.png")));
    
    // ---------------- Water ----------------
    std::pair<vector<Vertex>, vector<unsigned int>> _generatedWaterPlane = generatePlane(100, 100, 500, 500);
    std::vector<Texture> _waterTextures;
    unsigned int waterTextureId = TextureFromFile("water.png", "res");
    _waterTextures.push_back(Texture(waterTextureId, string("texture_diffuse"), string("res/water.png")));
    _waterTextures.push_back(Texture(waterTextureSpecId, string("texture_specular"), string("res/water_spec.png")));
    Mesh water = Mesh(_generatedWaterPlane.first, _generatedWaterPlane.second, std::move(_waterTextures));

    // --------------- Name ------------------
    std::pair<vector<Vertex>, vector<unsigned int>> _genNamePlane = generatePlane(1.0, 1.0, 4, 4);
    unsigned int nameTexId = TextureFromFile("name.png", "res");
    std::vector<Texture> _nameTextures;
    _nameTextures.push_back(Texture(nameTexId, string("texture_diffuse"), string("res/name.png")));
    Mesh name = Mesh(_genNamePlane.first, _genNamePlane.second, std::move(_nameTextures));

    // ------------- Main Island -------------
    std::pair<vector<Vertex>, vector<unsigned int>> _generatedIslandPlane = generatePlane(3.0, 3.0, 100, 100);
    _generatedIslandPlane = bublePlain(_generatedIslandPlane, 0, 0, 1.5, 1.5);
    _generatedIslandPlane = bublePlain(_generatedIslandPlane, 0.5, 0.5, 1.0, 1.4);
    std::vector<Texture> _sandTextures;
    unsigned int sandTextureId = TextureFromFile("sand.png", "res");
    unsigned int sandTextureSpecId = TextureFromFile("sand_spec.png", "res");
    _sandTextures.push_back(Texture(sandTextureId, string("texture_diffuse"), string("res/sand.png")));
    _sandTextures.push_back(Texture(sandTextureSpecId, string("texture_specular"), string("res/sand_spec.png")));
    Mesh sand = Mesh(_generatedIslandPlane.first, _generatedIslandPlane.second, std::move(_sandTextures));

    // -------------- Island 2 ---------------
    std::pair<vector<Vertex>, vector<unsigned int>> _generatedIsland2Plane = generatePlane(3.0, 3.0, 100, 100);
    _generatedIsland2Plane = bublePlain(_generatedIsland2Plane, 0, 0, 1.7, 1.7);
    std::vector<Texture> _sand2Textures;
    unsigned int sand2TextureId = TextureFromFile("sand.png", "res");
    unsigned int sand2TextureSpecId = TextureFromFile("sand_spec.png", "res");
    _sand2Textures.push_back(Texture(sand2TextureId, string("texture_diffuse"), string("res/sand.png")));
    _sand2Textures.push_back(Texture(sand2TextureSpecId, string("texture_specular"), string("res/sand_spec.png")));
    Mesh sand2 = Mesh(_generatedIsland2Plane.first, _generatedIsland2Plane.second, std::move(_sand2Textures));

    // ---------------- Sun ------------------
    std::pair<vector<Vertex>, vector<unsigned int>> _generatedSunPlane = generateSphere(1.0, 30, 30);
    std::vector<Texture> _sunTextures;
    unsigned int sunTextureId = TextureFromFile("8k_sun.png", "res");
    unsigned int sunTextureSpecId = TextureFromFile("8k_sun.png", "res");
    _sunTextures.push_back(Texture(sunTextureId, string("texture_diffuse"), string("res/8k_sun.png")));
    _sunTextures.push_back(Texture(sunTextureSpecId, string("texture_specular"), string("res/8k_sun.png")));
    Mesh sun = Mesh(_generatedSunPlane.first, _generatedSunPlane.second, std::move(_sunTextures));

    // --------------- Moon ------------------
    std::pair<vector<Vertex>, vector<unsigned int>> _generatedMoonPlane = generateSphere(3, 30, 30);
    std::vector<Texture> _moonTextures;
    unsigned int moonTextureId = TextureFromFile("8k_moon.png", "res");
    unsigned int moonTextureSpecId = TextureFromFile("8k_moon.png", "res");
    _moonTextures.push_back(Texture(moonTextureId, string("texture_diffuse"), string("res/8k_moon.png")));
    _moonTextures.push_back(Texture(moonTextureSpecId, string("texture_specular"), string("res/8k_moon.png")));
    Mesh moon = Mesh(_generatedMoonPlane.first, _generatedMoonPlane.second, std::move(_moonTextures));

    // --------------- Cloud ------------------
    std::pair<vector<Vertex>, vector<unsigned int>> _generatedCloudPlane = generateSphere(4, 4, 4);
    std::vector<Texture> _cloudTextures;
    unsigned int clopudTextureId = TextureFromFile("cloud.png", "res");
    unsigned int clopudTextureSpecId = TextureFromFile("cloud_spec.png", "res");
    _cloudTextures.push_back(Texture(clopudTextureId, string("texture_diffuse"), string("res/cloud.png")));
    _cloudTextures.push_back(Texture(clopudTextureSpecId, string("texture_specular"), string("res/cloud_spec.png")));
    Mesh cloud = Mesh(_generatedCloudPlane.first, _generatedCloudPlane.second, std::move(_cloudTextures));

    // ---------------- Sky ------------------
    std::pair<vector<Vertex>, vector<unsigned int>> _generatedSkyPlane = generateSphere(50, 200, 200, true);
    std::vector<Texture> _skyTextures;
    unsigned int skyTextureId = TextureFromFile("starmap_2020_4k_print.jpg", "res");
    unsigned int skyTextureSpecId = TextureFromFile("sky_ambient.png", "res");
    _skyTextures.push_back(Texture(skyTextureId, string("texture_diffuse"), string("res/starmap_2020_4k_print.jpg")));
    _skyTextures.push_back(Texture(skyTextureSpecId, string("texture_specular"), string("res/sky_ambient.png")));
    Mesh sky = Mesh(_generatedSkyPlane.first, _generatedSkyPlane.second, std::move(_skyTextures));

    // --------------- Fire ------------------
    std::pair<vector<Vertex>, vector<unsigned int>> _generatedFirePlane = generateCube(1.0, 1.0, 1.0);
    std::vector<Texture> _fireTextures;
    unsigned int fireTextureId = TextureFromFile("fire.png", "res");
    _fireTextures.push_back(Texture(fireTextureId, string("texture_diffuse"), string("res/fire.png")));
    _fireTextures.push_back(Texture(clopudTextureSpecId, string("texture_specular"), string("res/cloud_spec.png")));
    Mesh fire = Mesh(_generatedFirePlane.first, _generatedFirePlane.second, std::move(_fireTextures));

    Model shark("res/shark/SHARK.obj");
    Model palm("res/palm/palm.obj");
    Model flashlight("res/torch_final/newer.obj");
    Model campfire("res/Campfire/Campfire.obj");

    //Tjemena i baferi su definisani u model klasi i naprave se pri stvaranju objekata

    Shader unifiedShader("basic.vert", "basic.frag");

    //Render petlja
    unifiedShader.use();
    unifiedShader.setVec3("uLightPos", 0, 20, 3);
    unifiedShader.setVec3("uViewPos", 0, 0, 5);
    unifiedShader.setVec3("uLightColor", 1, 1, 1);
    glm::vec3 eye = glm::vec3(0.0f, 3.0f, 10.0f);
    glm::vec3 center = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::mat4 view = glm::lookAt(eye, center, up);
    unifiedShader.setMat4("uV", view);

    glm::mat4 projectionP = glm::perspective(glm::radians(45.0f), (float)wWidth / (float)wHeight, 0.05f, 200.0f);
    glm::mat4 projectionO = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, 0.05f, 200.0f);

    glm::vec3 eyeName = glm::vec3(0.0f, 5.0f, 0.1f);
    glm::mat4 vName = glm::lookAt(eyeName, center, up);
    glm::mat4 pName = glm::perspective(glm::radians(45.0f), (float)wWidth / (float)wHeight, 0.05f, 200.0f);

    float orthoScale = 10.0f;
    unifiedShader.setMat4("uP", projectionP);

    // How island irritates water                          X     Z       R     H
    unifiedShader.setVec4("uWaterIrritants[0]", glm::vec4( 0.0,  0.0,   1.5, 1.5));
    unifiedShader.setVec4("uWaterIrritants[1]", glm::vec4( 0.5,  0.5,   1.0, 0.5));
    unifiedShader.setVec4("uWaterIrritants[4]", glm::vec4( 10.0, -20.0, 1.5, 0.5));
    unifiedShader.setFloat("uWavingFrequency", 10.0);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    float prevTime = 0.0f;
    bool isometric = false;
    int flags = BLINN | WATER_SHADING;
    unifiedShader.setInt("uFlags", flags);

    unifiedShader.setFloat("pointLights[0].constant", 1.0f);
    unifiedShader.setFloat("pointLights[0].linear", 0.09f);
    unifiedShader.setFloat("pointLights[0].quadratic", 0.032f);
    unifiedShader.setVec3("pointLights[0].position", glm::vec3(0.8, -0.3, 0.8));
    unifiedShader.setVec3("pointLights[0].lightColor", glm::vec3(0.9f, 0.5f, 0.0f));
    unifiedShader.setVec3("pointLights[0].direction", glm::vec3(0.0f, 1.0f, 0.0f));
    unifiedShader.setFloat("pointLights[0].cutOff", glm::radians(-90.0f));
    unifiedShader.setFloat("pointLights[0].outerCutOff", glm::radians(-90.0f));

    unifiedShader.setFloat("pointLights[1].constant", 1.0f);
    unifiedShader.setFloat("pointLights[1].linear", 0.09f);
    unifiedShader.setFloat("pointLights[1].quadratic", 0.032f);
    unifiedShader.setVec3("pointLights[1].position", glm::vec3(0.0, 0.0, 0.0));
    unifiedShader.setVec3("pointLights[1].lightColor", glm::vec3(0.9f, 0.0f, 0.9f));
    unifiedShader.setVec3("pointLights[1].direction", glm::vec3(0.0f, 1.0f, 0.0f));
    unifiedShader.setFloat("pointLights[1].cutOff", glm::radians(55.0f));
    unifiedShader.setFloat("pointLights[1].outerCutOff", glm::radians(50.0f));

    while (!glfwWindowShouldClose(window))
    {
        float t = glfwGetTime();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        handleInput(window, unifiedShader, eye, center, up, projectionP, projectionO, t - prevTime, isometric, orthoScale, flags, t);
        view = glm::lookAt(eye, center, up);
        unifiedShader.setVec3("uAmbient", glm::vec3(0.05, 0.05, 0.05));
        unifiedShader.setInt("uFlags", flags);
        unifiedShader.setVec3("uViewPos", eye);
        unifiedShader.setMat4("uV", view);
        if (isometric) unifiedShader.setMat4("uP", projectionO); else unifiedShader.setMat4("uP", projectionP);
        unifiedShader.setMat4(
            "uM", glm::mat4(1.0f)
        );

        unifiedShader.setVec3("uSkyColor", skyFromTime(t));
        unifiedShader.setInt("uFlags", flags | SKY_MODE);
        sky.Draw(unifiedShader);
        unifiedShader.setVec3("uSkyColor", glm::vec3(0.0f));

        unifiedShader.setMat4(
            "uM",
            glm::rotate(
                glm::mat4(1.0f),
                (float)t / TIME_SPEED + TIME_PHAZE,
                glm::normalize(glm::vec3(1.0f, 1.0f, 0.0f))
            ) *
            glm::translate(
                glm::mat4(1.0f),
                glm::vec3(0.0, 0.0, 50.0)
            )
        );
        unifiedShader.setVec3("uLightPos", glm::rotate(
            glm::mat4(1.0f),
            (float)t / TIME_SPEED + TIME_PHAZE,
            glm::normalize(glm::vec3(1.0f, 1.0f, 0.0f))
        ) *
            glm::vec4(0.0f, 0.0f, 50.0f, 1.0f)
        );
        unifiedShader.setVec3("uLightColor", lightFromTime(t));
        unifiedShader.setVec3("uAmbient", glm::vec3(1.0f));
        sun.Draw(unifiedShader);
        unifiedShader.setVec3("uAmbient", glm::vec3(0.05, 0.05, 0.05));
        unifiedShader.setInt("uFlags", flags);

        unifiedShader.setMat4(
            "uM",
            glm::rotate(
                glm::mat4(1.0f),
                (float)t / TIME_SPEED + TIME_PHAZE - glm::radians(180.0f),
                glm::normalize(glm::vec3(1.0f, 1.0f, 0.0f))
            )*
            glm::translate(
                glm::mat4(1.0f),
                glm::vec3(00.0, 0.0, 50.0)
            )
        );
        unifiedShader.setVec3("uAmbient", glm::vec3(1.0f));
        moon.Draw(unifiedShader);
        unifiedShader.setVec3("uAmbient", glm::vec3(0.05, 0.05, 0.05));

        unifiedShader.setMat4(
            "uM",
            glm::translate(
                glm::mat4(1.0f),
                glm::vec3(0.0, 0.0, 0.0)
            ) * glm::scale(
                glm::mat4(1.0f),
                glm::vec3(0.02, 0.02, 0.02)
            ) * glm::rotate(
				glm::mat4(1.0f),
                (float)t / 4.0f - 0.5f,
                glm::vec3(0.0f, 1.0f, 0.0f)
			)
        );
        unifiedShader.setVec3("pointLights[1].direction", 
            glm::rotate(
                glm::mat4(1.0f),
                (float)t / 4.0f - 0.5f,
                glm::vec3(0.0f, 1.0f, 0.0f)
            ) * glm::vec4(1.0f, 0.0f, 0.0f, 1.0f)
        );
        flashlight.Draw(unifiedShader);

        // Sharks
        glm::mat4 model = 
            glm::rotate(glm::mat4(1.0f), (float)t / 4.0f, glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::translate(glm::mat4(1.0f), glm::vec3(3.5, -2.0, 3.5)) *
            glm::rotate(glm::mat4(1.0f), glm::radians(270.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::rotate(glm::mat4(1.0f), glm::radians(35.0f) - std::max(0.0f, (float)std::sin(t) / 1.5f), glm::vec3(0.0f, 0.0f, 1.0f));
        unifiedShader.setMat4("uM", model);
        shark.Draw(unifiedShader);

        model =
            glm::rotate(glm::mat4(1.0f), (float)t / 4.0f + glm::radians(130.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::translate(glm::mat4(1.0f), glm::vec3(8.0, -2.0, 3.5)) *
            glm::rotate(glm::mat4(1.0f), glm::radians(270.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::rotate(glm::mat4(1.0f), glm::radians(35.0f) - std::max(0.0f, (float)std::sin(t * 1.2f + 1.0f) / 1.5f), glm::vec3(0.0f, 0.0f, 1.0f));
        unifiedShader.setMat4("uM", model);
        shark.Draw(unifiedShader);

        // How shark irritates water
        unifiedShader.setFloat("uWavePhaze", t * 4.0);
        unifiedShader.setFloat("uWaveAmplitude", 0.1);
        glm::vec3 sharkPos =
            glm::rotate(glm::mat4(1.0f), (float)t / 4.0f, glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::vec4(4.0, 0.0, 3.0, 1.0);
        unifiedShader.setVec4(
            "uWaterIrritants[2]",
            glm::vec4(
                sharkPos.x, sharkPos.z, 
                std::max(0.0f, (float)std::sin(t) / 1.5f), 
                0.5 + std::max(0.0f, (float)std::sin(t) / 1.5f)
            )
        );
        sharkPos =
            glm::rotate(glm::mat4(1.0f), (float)t / 4.0f + glm::radians(130.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::vec4(8.5, 0.0, 3.0, 1.0);
        unifiedShader.setVec4(
            "uWaterIrritants[3]",
            glm::vec4(
                sharkPos.x, sharkPos.z,
                std::max(0.0f, (float)std::sin(t * 1.2f + 1.0f) / 1.5f),
                0.5 + std::max(0.0f, (float)std::sin(t * 1.2f + 1.0f) / 1.5f)
            )
        );

        // Slightly dowm
        unifiedShader.setMat4("uM", glm::translate(glm::mat4(1.0f), glm::vec3(0.0, -1.0, 0.0)));
        water.Draw(unifiedShader);

        unifiedShader.setFloat("uWaveAmplitude", 0.0);

        // Slightly down (a bit more then water)
        unifiedShader.setMat4("uM", glm::translate(glm::mat4(1.0f), glm::vec3(0.0, -1.5, 0.0)));
        sand.Draw(unifiedShader);

        unifiedShader.setMat4("uM", glm::translate(glm::mat4(1.0f), glm::vec3(10.0, -1.85, -20.0)));
        sand2.Draw(unifiedShader);

        glDisable(GL_CULL_FACE);

        unifiedShader.setMat4("uM", glm::translate(glm::mat4(1.0f), glm::vec3(0.15, 0.7, -0.45)));
        palm.Draw(unifiedShader);
        unifiedShader.setMat4("uM", 
            glm::translate(glm::mat4(1.0f), glm::vec3(10, 0.8, -20.2)) *
            glm::rotate(
                glm::mat4(1.0f),
                glm::radians(15.0f),
                glm::vec3(1.0f, 0.0f, 0.0f)
            ) *
            glm::rotate(
                glm::mat4(1.0f),
                glm::radians(15.0f),
                glm::vec3(0.0f, 0.0f, 1.0f)
            )
        );
        palm.Draw(unifiedShader);
        unifiedShader.setMat4("uM", 
            glm::translate(glm::mat4(1.0f), glm::vec3(9.5, 0.6, -20.4)) *
            glm::rotate(
                glm::mat4(1.0f),
                glm::radians(40.0f),
                glm::vec3(0.0f, 0.0f, 1.0f)
            )
        );
        palm.Draw(unifiedShader);

        glEnable(GL_CULL_FACE);

        unifiedShader.setMat4("uM",
            glm::translate(glm::mat4(1.0f), glm::vec3(0.8, -0.67, 0.8)) * 
            glm::scale(
                glm::mat4(1.0f),
                glm::vec3(0.5, 0.5, 0.5)
            ) *
            glm::rotate(
                glm::mat4(1.0f),
                glm::radians(25.0f),
                glm::normalize(glm::vec3(1.0f, 0.0f, -1.0f))
            )
        );
        campfire.Draw(unifiedShader);

        glDisable(GL_CULL_FACE);
        unifiedShader.setVec3("uAmbient", glm::vec3(1.0f));

        for (int i = 0; i < 40; i++) {
            float r1 = std::sin(i * 123345) / 10.0;
            float r2 = std::sin(i * 223345) / 3.0;
            float r3 = std::sin(i * 323345) / 10.0;
            float r4 = std::sin(i * 423345) * 2.0;
            float r5 = std::sin(i * 523345) * 2.0;

            unifiedShader.setMat4("uM",
                glm::translate(glm::mat4(1.0f), glm::vec3(0.8 + r1, -0.67 + std::sinf(std::fmodf(t + r4, glm::radians(90.0))) / 3.0 + r2, 0.8 + r3)) *
                glm::scale(
                    glm::mat4(1.0f),
                    glm::vec3(0.05 * std::sinf(t * 4 + r5), 0.05 * std::sinf(t * 4 + r5), 0.05 * std::sinf(t * 4 + r5))
                ) *
                glm::rotate(
                    glm::mat4(1.0f),
                    glm::radians(45.0f),
                    glm::normalize(glm::vec3(1.0f, 0.0f, 0.0f))
                ) *
                glm::rotate(
                    glm::mat4(1.0f),
                    glm::radians(45.0f),
                    glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f))
                )
            );
            fire.Draw(unifiedShader);
        }

        glEnable(GL_CULL_FACE);
        unifiedShader.setVec3("uAmbient", glm::vec3(0.05, 0.05, 0.05));
        unifiedShader.setMat4("uM", glm::mat4(1.0f));

        // Cloud 1
        glm::mat4 cloudModel = glm::translate(glm::mat4(1.0f), glm::vec3(cloudShift(t, 100.0f), 10.0, -10.0));
        glm::mat4 cloudLarge = glm::scale(glm::mat4(1.0f), glm::vec3(1.0));
        glm::mat4 cloudMidLarge = glm::scale(glm::mat4(1.0f), glm::vec3(0.85));
        glm::mat4 cloudMedium = glm::scale(glm::mat4(1.0f), glm::vec3(0.75));
        glm::mat4 cloudSmall = glm::scale(glm::mat4(1.0f), glm::vec3(0.5));
        glm::mat4 cloud2 = glm::rotate(glm::mat4(1.0f), 1.0f, glm::vec3(0.0f, 1.0f, 0.0f));
        glm::mat4 cloud3 = glm::rotate(glm::mat4(1.0f), 1.0f, glm::vec3(1.0f, 0.0f, 0.0f));
        glm::mat4 cloud4 = glm::rotate(glm::mat4(1.0f), 1.0f, glm::vec3(0.0f, 0.0f, 1.0f));

        unifiedShader.setMat4("uM", cloudModel * cloudMedium * cloud2);
        cloud.Draw(unifiedShader);
        unifiedShader.setMat4("uM", cloudModel * cloudMidLarge * cloud3);
        cloud.Draw(unifiedShader);
        unifiedShader.setMat4("uM", cloudModel);
        cloud.Draw(unifiedShader);
        cloudModel = cloudModel * glm::translate(glm::mat4(1.0f), glm::vec3(4.0, 0.0, -4.0));
        unifiedShader.setMat4("uM", cloudModel * cloudMedium * cloud2);
        cloud.Draw(unifiedShader);
        unifiedShader.setMat4("uM", cloudModel* cloudMidLarge* cloud3);
        cloud.Draw(unifiedShader);

        // Cloud 2
        cloudModel = glm::translate(glm::mat4(1.0f), glm::vec3(cloudShift(t, 120.0f), 10.0, -30.0));
        unifiedShader.setMat4("uM", cloudModel* cloudMedium* cloud2);
        cloud.Draw(unifiedShader);
        unifiedShader.setMat4("uM", cloudModel* cloudMidLarge* cloud3);
        cloud.Draw(unifiedShader);
        unifiedShader.setMat4("uM", cloudModel);
        cloud.Draw(unifiedShader);
        cloudModel = cloudModel * glm::translate(glm::mat4(1.0f), glm::vec3(-4.0, 0.0, 1.0));
        unifiedShader.setMat4("uM", cloudModel* cloudMedium* cloud2);
        cloud.Draw(unifiedShader);
        unifiedShader.setMat4("uM", cloudModel* cloudMidLarge* cloud3);
        cloud.Draw(unifiedShader);

        // Cloud 3
        cloudModel = glm::translate(glm::mat4(1.0f), glm::vec3(cloudShift(t, 90.0f), 10.0, 20.0));
        unifiedShader.setMat4("uM", cloudModel* cloudMedium* cloud2);
        cloud.Draw(unifiedShader);
        unifiedShader.setMat4("uM", cloudModel* cloudMidLarge* cloud3);
        cloud.Draw(unifiedShader);
        unifiedShader.setMat4("uM", cloudModel);
        cloud.Draw(unifiedShader);
        cloudModel = cloudModel * glm::translate(glm::mat4(1.0f), glm::vec3(-4.0, 0.0, 1.0));
        unifiedShader.setMat4("uM", cloudModel* cloudSmall * cloud2);
        cloud.Draw(unifiedShader);
        unifiedShader.setMat4("uM", cloudModel* cloudMedium * cloud3);
        cloud.Draw(unifiedShader);



        //glm::mat4 cloudModel = glm::translate(glm::mat4(1.0f), glm::vec3(10.0, 20.0, -10.0));
        //unifiedShader.setMat4("uM", cloudModel* glm::scale(glm::mat4(1.0f), glm::vec3(1.0))* glm::rotate(glm::mat4(1.0f), 0.0f, glm::vec3(0.0f, 1.0f, 0.0f)));
        //cloud.Draw(unifiedShader);

        unifiedShader.setMat4("uM", glm::mat4(1.0f));

        glClear(GL_DEPTH_BUFFER_BIT);
        prevTime = t;
        unifiedShader.setInt("uFlags", flags | NAME_MODE);
        unifiedShader.setVec3("uViewPos", eyeName);
        unifiedShader.setMat4("uV", vName);
        unifiedShader.setMat4("uP", pName);
        unifiedShader.setMat4(
            "uM",
            glm::translate(
                glm::mat4(1.0f),
                glm::vec3(-2.9, 0.0, -2.0)
            )
        );
        name.Draw(unifiedShader);
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void handleInput(
    GLFWwindow* window, Shader& unifiedShader, glm::vec3& eye, glm::vec3& center, glm::vec3& up, glm::mat4& projectionP, glm::mat4& projectionO,
    const float deltaTime, bool& isometric, float& orthoScale, int& flags, float& t
)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }

    // ----------------- Camera -----------------
    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
    {
        eye = glm::vec3(0.0f, 3.0f, 10.0f);
        center = glm::vec3(0.0f, 0.0f, 0.0f);
        unifiedShader.setMat4("uP", projectionP);
        isometric = false;
    }
    if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
    {
        eye = glm::vec3(0.0f, 15.0f, 15.0f);
        center = glm::vec3(0.0f, 0.0f, 0.0f);
        orthoScale = 10.0f;
        projectionO = glm::ortho(-orthoScale, orthoScale, -orthoScale, orthoScale, 0.05f, 200.0f);
        unifiedShader.setMat4("uP", projectionO);
        isometric = true;
    }

    // ------------------ Debug -----------------
    if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
        glPointSize(8.0);
    }

    if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glLineWidth(4.0);
        glPointSize(4.0);
    }

    if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glPointSize(4.0);
    }

    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
    {
        glfwSetTime(0);
    }

    if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS && std::abs(t - lastSwitch) > TOGLE)
    {
        flags = flags ^ SHOW_NORMALS;
		unifiedShader.setInt("uFlags", flags);
        lastSwitch = t;
    }
    if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS && std::abs(t - lastSwitch) > TOGLE)
    {
		flags = flags ^ SHOW_GRID;
        unifiedShader.setInt("uFlags", flags);
        lastSwitch = t;
    }
    if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS && std::abs(t - lastSwitch) > TOGLE)
	{
		flags = flags ^ SHOW_CUTOFF;
		unifiedShader.setInt("uFlags", flags);
        lastSwitch = t;
	}
    if (glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS && std::abs(t - lastSwitch) > TOGLE)
    {
        flags = flags ^ WATER_SHADING;
        unifiedShader.setInt("uFlags", flags);
        lastSwitch = t;
    }

    // ----------------- Movement ----------------
    if (!isometric) {
        glm::vec3 forward = glm::normalize(center - eye);
        glm::vec3 localUp = glm::cross(glm::cross(forward, up), forward);
        glm::vec3 right = glm::normalize(glm::cross(forward, localUp));

        float ANGLE_SPEED = 0.01f;
        float SPEED = deltaTime * 5.0f;

        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        {
            eye -= right * SPEED;
            center -= right * SPEED;
        }
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        {
            eye += right * SPEED;
            center += right * SPEED;
        }
        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        {
            eye += forward * SPEED;
            center += forward * SPEED;
        }
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        {
            eye -= forward * SPEED;
            center -= forward * SPEED;
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        {
            eye += localUp * SPEED;
            center += localUp * SPEED;
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
        {
            eye -= localUp * SPEED;
            center -= localUp * SPEED;
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
        {
            center = eye + glm::vec3(glm::rotate(glm::mat4(1.0f), ANGLE_SPEED, localUp) * glm::vec4(center - eye, 0.0));
        }
        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        {
            center = eye + glm::vec3(glm::rotate(glm::mat4(1.0f), -ANGLE_SPEED, localUp) * glm::vec4(center - eye, 0.0));
        }
        if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
        {
			center = eye + glm::vec3(glm::rotate(glm::mat4(1.0f), ANGLE_SPEED, right) * glm::vec4(center - eye, 0.0));
		}
        if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
        {
           	center = eye + glm::vec3(glm::rotate(glm::mat4(1.0f), -ANGLE_SPEED, right) * glm::vec4(center - eye, 0.0));
        }
    }
    else {
        glm::vec3 sight = glm::normalize(center - eye);
        glm::vec3 forward = glm::normalize(glm::vec3(sight.x, 0.0f, sight.z));
        glm::vec3 right = glm::normalize(glm::cross(forward, glm::vec3(0.0f, 1.0f, 0.0f)));

        float ANGLE_SPEED = 0.01f;
        float SPEED = deltaTime * 5.0f;

        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        {
            eye -= right * SPEED;
            center -= right * SPEED;
        }
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        {
            eye += right * SPEED;
            center += right * SPEED;
        }
        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        {
            eye += forward * SPEED;
            center += forward * SPEED;
        }
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        {
            eye -= forward * SPEED;
            center -= forward * SPEED;
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        {
            if (orthoScale > 1.0f)
            {
				orthoScale -= 0.1f;
                projectionO = glm::ortho(-orthoScale, orthoScale, -orthoScale, orthoScale, 6.0f, 200.0f);
			}
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
        {
            if (orthoScale < 20.0f)
            {
                orthoScale += 0.1f;
                projectionO = glm::ortho(-orthoScale, orthoScale, -orthoScale, orthoScale, 0.05f, 300.0f);
            }
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
        {
            eye = center + glm::vec3(glm::rotate(glm::mat4(1.0f), ANGLE_SPEED, glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(eye - center, 0.0));
        }
        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        {
            eye = center + glm::vec3(glm::rotate(glm::mat4(1.0f), -ANGLE_SPEED, glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(eye - center, 0.0));
        }
        if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
        {
            if (glm::dot(sight, glm::vec3(0.0f, 1.0f, 0.0f)) < 0.9f)
            {
				eye = center + glm::vec3(glm::rotate(glm::mat4(1.0f), ANGLE_SPEED, right) * glm::vec4(eye - center, 0.0));
			}
        }
        if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
        {
            if (glm::dot(sight, glm::vec3(0.0f, 1.0f, 0.0f)) > -0.9f)
            {
                eye = center + glm::vec3(glm::rotate(glm::mat4(1.0f), -ANGLE_SPEED, right) * glm::vec4(eye - center, 0.0));
            }
        }
    }
}
